﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    abstract class Character
    {
        protected StoredItems bag;
        protected EquippedItems equipped;
        protected string name;
        protected int currentHealth;
        protected int maxHealth;
        protected bool dead;

        public StoredItems Bag { get { return bag; } }
        public EquippedItems Equipped { get { return equipped; } }
        public string Name { get { return name; } }
        public int CurrentHealth { get { return currentHealth; } }
        public int MaxHealth { get { return maxHealth; } }
        public bool IsDead { get { return dead; } }

        public Character()
        {
            dead = false;
            maxHealth = 100;
            currentHealth = maxHealth;
        }

        public int CalcTotalAttackValue()
        {
            int total = 0;
            for (int i = 0; i < 6; ++i)
            {
                if (Equipped[(InventorySlotId)i] is IWeapon)
                {
                    IWeapon weapon = (IWeapon)Equipped[(InventorySlotId)i];
                    total += weapon.AttackValue;
                }
            }
            return total;
        }

        public int CalcTotalDefenseValue()
        {
            int total = 0;
            for (int i = 0; i < 6; ++i)
            {
                if (Equipped[(InventorySlotId)i] is IArmor)
                {
                    IArmor weapon = (IArmor)Equipped[(InventorySlotId)i];
                    total += weapon.DefenseValue;
                }
            }
            return total;
        }

        public void TakeDamage(int damage)
        {
            currentHealth -= damage;
            if(currentHealth <= 0)
            {
                currentHealth = 0;
                dead = true;
            }
        }

        public int Heal(int healing)
        {
            int startingHealth = CurrentHealth;
            currentHealth += healing;
            if(currentHealth > MaxHealth)
            {
                currentHealth = MaxHealth;
            }
            return CurrentHealth - startingHealth;
        }
    }
}
