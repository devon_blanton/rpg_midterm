﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Bat : Enemy
    {
        public Bat()
        {
            name = "Bat";
            equipped = new EquippedItems(
                new NaturalWeapon(1, 3),
                new NaturalArmor(3, 3)
                );
        }
    }
}
