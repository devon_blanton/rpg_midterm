﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Dog : Enemy
    {
        public Dog()
        {
            name = "Dog";
            equipped = new EquippedItems(
                new NaturalWeapon(2, 4),
                new NaturalArmor(1, 1)
                );
        }
    }
}
