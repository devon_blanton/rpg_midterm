﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    abstract class Enemy : Character
    {
        public Enemy()
        {
            maxHealth = 20;
            currentHealth = maxHealth;
            bag = new StoredItems(7, 1);
        }
    }
}
