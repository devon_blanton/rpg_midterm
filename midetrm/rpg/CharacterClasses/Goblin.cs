﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Goblin : Enemy
    {
        public Goblin()
        {
            name = "Goblin";
            equipped = new EquippedItems(
                new IronSword(),
                new Gambeson()
                );
        }
    }
}
