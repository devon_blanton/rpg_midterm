﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Kobold : Enemy
    {
        public Kobold()
        {
            name = "Kobold";
            equipped = new EquippedItems(
                new BronzeSword(),
                new Gambeson()
                );
        }
    }
}
