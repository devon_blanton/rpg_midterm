﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    abstract class Miniboss : Enemy
    {
        public Miniboss()
        {
            maxHealth = 50;
            currentHealth = maxHealth;
            bag = new StoredItems(9, 3);
        }
    }
}
