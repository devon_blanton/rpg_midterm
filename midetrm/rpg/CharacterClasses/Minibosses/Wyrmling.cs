﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Wyrmling : Miniboss
    {
        public Wyrmling()
        {
            maxHealth = 50;
            currentHealth = maxHealth;
            name = "Wyrmling";
            equipped = new EquippedItems(
                new NaturalWeapon(15, 18),
                new NaturalArmor(7, 11)
                );
        }
    }
}
