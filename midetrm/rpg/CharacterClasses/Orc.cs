﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Orc : Enemy
    {
        public Orc()
        {
            name = "Orc";
            equipped = new EquippedItems(
                new IronSword(),
                new IronChestpiece()
                );
        }
    }
}
