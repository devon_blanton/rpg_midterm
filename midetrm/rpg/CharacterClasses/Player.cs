﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Player : Character
    {
        public Player()
        {
            name = "Player";
            bag = new StoredItems(20);
            equipped = new EquippedItems(
                new BronzeSword(),
                new Gambeson(),
                new SmallHealthPotion()
                );
        }
    }
}
