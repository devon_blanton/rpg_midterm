﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Rat : Enemy
    {
        public Rat()
        {
            name = "Rat";
            equipped = new EquippedItems(
                new NaturalWeapon(1,3),
                new NaturalArmor(0,0)
                );
        }
    }
}
