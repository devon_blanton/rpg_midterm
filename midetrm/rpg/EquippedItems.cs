﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class EquippedItems
    {
        Item[] slots;

        public EquippedItems(params Item[] items)
        {
            slots = new Item[6];
            if(items.Length <= 6)
            {
                foreach (Item item in items)
                {
                    slots[(int)item.Slot] = item;
                }
            }
        }


        public Item GetItem(InventorySlotId slot)
        {
            return slots[(int)slot];
        }


        public Item Equip(Item item)
        {
            Item returnItem = slots[(int)item.Slot];
            slots[(int)item.Slot] = item;
            Program.form1.InventoryUpdate();
            return returnItem;
        }

        public Item Unequip(InventorySlotId slot)
        {
            Item returnItem = slots[(int)slot];
            slots[(int)slot] = null;
            Program.form1.InventoryUpdate();
            return returnItem;
        }

        public Item this[InventorySlotId id]
        {
            get
            {
                return GetItem(id);
            }

            set
            {
                slots[(int)id] = value;
            }
        }
    }
}
