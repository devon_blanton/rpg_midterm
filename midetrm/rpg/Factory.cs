﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    static class Factory
    {
        static Random rand = new Random();

        public static Enemy RandomEnemyGenerator()
        {
            int num = rand.Next(1, 7);
            switch (num)
            {
                case 1:
                    return new Rat();
                case 2:
                    return new Dog();
                case 3:
                    return new Bat();
                case 4:
                    return new Kobold();
                case 5:
                    return new Goblin();
                case 6:
                    return new Orc();
                default:
                    return new Rat();
            }
        }

        public static Enemy RandomMinibossGenerator()
        {
            int num = rand.Next(1, 1);
            switch (num)
            {
                case 1:
                    return new Wyrmling();
                default:
                    return new Rat();
            }
        }

        public static Item RandomItemGenerator()
        {
            int num = rand.Next(1, 19);

            switch (num)
            {
                case 1:
                    return new BronzeSword();
                case 2:
                    return new IronSword();
                case 3:
                    return new SteelSword();
                case 4:
                    return new Buckler();
                case 5:
                    return new Gambeson();
                case 6:
                    return new IronChestpiece();
                case 7:
                    return new SteelChestpiece();
                case 8:
                    return new BronzeHelmet();
                case 9:
                    return new IronHelmet();
                case 10:
                    return new SteelHelmet();
                case 11:
                    return new BronzeGrieves();
                case 12:
                    return new IronGrieves();
                case 13:
                    return new SteelGrieves();
                case 14:
                    return new BronzeVambraces();
                case 15:
                    return new IronVambraces();
                case 16:
                    return new SteelVambraces();
                case 17:
                    return new SmallHealthPotion();
                case 18:
                    return new LargeHealthPotion();
                default:
                    return new SmallHealthPotion();
            }
        }
        

    }
}
