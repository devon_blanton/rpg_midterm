﻿namespace rpg
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.depthLabel = new System.Windows.Forms.Label();
            this.playerLabel = new System.Windows.Forms.Label();
            this.enemyLabel = new System.Windows.Forms.Label();
            this.playerHealthLabel = new System.Windows.Forms.Label();
            this.enemyHealthLabel = new System.Windows.Forms.Label();
            this.playerHealthBar = new System.Windows.Forms.ProgressBar();
            this.enemyHealthBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.attackButton = new System.Windows.Forms.Button();
            this.potionButton = new System.Windows.Forms.Button();
            this.nextEnemyButton = new System.Windows.Forms.Button();
            this.inventoryButton = new System.Windows.Forms.Button();
            this.playerDamageTakenLabel = new System.Windows.Forms.Label();
            this.enemyDamageTakenLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.enemyInventoryLabel = new System.Windows.Forms.Label();
            this.playerEquipmentListBox = new System.Windows.Forms.ListBox();
            this.enemyEquipmentListBox = new System.Windows.Forms.ListBox();
            this.playerBagCapacity = new System.Windows.Forms.Label();
            this.takeItemButton = new System.Windows.Forms.Button();
            this.killsLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // depthLabel
            // 
            this.depthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.depthLabel.Location = new System.Drawing.Point(583, 95);
            this.depthLabel.Name = "depthLabel";
            this.depthLabel.Size = new System.Drawing.Size(177, 34);
            this.depthLabel.TabIndex = 0;
            this.depthLabel.Text = "Depth: 1";
            this.depthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerLabel
            // 
            this.playerLabel.Location = new System.Drawing.Point(12, 15);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(177, 57);
            this.playerLabel.TabIndex = 0;
            this.playerLabel.Text = "Player";
            this.playerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enemyLabel
            // 
            this.enemyLabel.Location = new System.Drawing.Point(1154, 15);
            this.enemyLabel.Name = "enemyLabel";
            this.enemyLabel.Size = new System.Drawing.Size(177, 57);
            this.enemyLabel.TabIndex = 0;
            this.enemyLabel.Text = "Enemy";
            this.enemyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerHealthLabel
            // 
            this.playerHealthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerHealthLabel.Location = new System.Drawing.Point(12, 102);
            this.playerHealthLabel.Name = "playerHealthLabel";
            this.playerHealthLabel.Size = new System.Drawing.Size(177, 32);
            this.playerHealthLabel.TabIndex = 0;
            this.playerHealthLabel.Text = "100";
            this.playerHealthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enemyHealthLabel
            // 
            this.enemyHealthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyHealthLabel.Location = new System.Drawing.Point(1154, 102);
            this.enemyHealthLabel.Name = "enemyHealthLabel";
            this.enemyHealthLabel.Size = new System.Drawing.Size(177, 32);
            this.enemyHealthLabel.TabIndex = 0;
            this.enemyHealthLabel.Text = "100";
            this.enemyHealthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerHealthBar
            // 
            this.playerHealthBar.BackColor = System.Drawing.Color.Black;
            this.playerHealthBar.Location = new System.Drawing.Point(12, 76);
            this.playerHealthBar.Name = "playerHealthBar";
            this.playerHealthBar.Size = new System.Drawing.Size(177, 23);
            this.playerHealthBar.Step = 1;
            this.playerHealthBar.TabIndex = 1;
            this.playerHealthBar.Value = 100;
            // 
            // enemyHealthBar
            // 
            this.enemyHealthBar.BackColor = System.Drawing.Color.Black;
            this.enemyHealthBar.Location = new System.Drawing.Point(1154, 76);
            this.enemyHealthBar.Name = "enemyHealthBar";
            this.enemyHealthBar.Size = new System.Drawing.Size(177, 23);
            this.enemyHealthBar.Step = 1;
            this.enemyHealthBar.TabIndex = 1;
            this.enemyHealthBar.Value = 100;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(503, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 64);
            this.label1.TabIndex = 0;
            this.label1.Text = "The Dungeon";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // attackButton
            // 
            this.attackButton.Location = new System.Drawing.Point(466, 455);
            this.attackButton.Name = "attackButton";
            this.attackButton.Size = new System.Drawing.Size(167, 60);
            this.attackButton.TabIndex = 2;
            this.attackButton.Text = "Attack";
            this.attackButton.UseVisualStyleBackColor = true;
            this.attackButton.Click += new System.EventHandler(this.attackButton_Click);
            // 
            // potionButton
            // 
            this.potionButton.Location = new System.Drawing.Point(639, 455);
            this.potionButton.Name = "potionButton";
            this.potionButton.Size = new System.Drawing.Size(238, 60);
            this.potionButton.TabIndex = 2;
            this.potionButton.Text = "Take Potion";
            this.potionButton.UseVisualStyleBackColor = true;
            this.potionButton.Click += new System.EventHandler(this.potionButton_Click);
            // 
            // nextEnemyButton
            // 
            this.nextEnemyButton.Location = new System.Drawing.Point(466, 426);
            this.nextEnemyButton.Name = "nextEnemyButton";
            this.nextEnemyButton.Size = new System.Drawing.Size(218, 60);
            this.nextEnemyButton.TabIndex = 2;
            this.nextEnemyButton.Text = "Next Enemy";
            this.nextEnemyButton.UseVisualStyleBackColor = true;
            this.nextEnemyButton.Visible = false;
            this.nextEnemyButton.Click += new System.EventHandler(this.nextEnemyButton_Click);
            // 
            // inventoryButton
            // 
            this.inventoryButton.Location = new System.Drawing.Point(690, 426);
            this.inventoryButton.Name = "inventoryButton";
            this.inventoryButton.Size = new System.Drawing.Size(187, 60);
            this.inventoryButton.TabIndex = 2;
            this.inventoryButton.Text = "Inventory";
            this.inventoryButton.UseVisualStyleBackColor = true;
            this.inventoryButton.Visible = false;
            this.inventoryButton.Click += new System.EventHandler(this.inventoryButton_Click);
            // 
            // playerDamageTakenLabel
            // 
            this.playerDamageTakenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerDamageTakenLabel.ForeColor = System.Drawing.Color.Black;
            this.playerDamageTakenLabel.Location = new System.Drawing.Point(195, 34);
            this.playerDamageTakenLabel.Name = "playerDamageTakenLabel";
            this.playerDamageTakenLabel.Size = new System.Drawing.Size(108, 100);
            this.playerDamageTakenLabel.TabIndex = 0;
            this.playerDamageTakenLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enemyDamageTakenLabel
            // 
            this.enemyDamageTakenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyDamageTakenLabel.ForeColor = System.Drawing.Color.Black;
            this.enemyDamageTakenLabel.Location = new System.Drawing.Point(1040, 34);
            this.enemyDamageTakenLabel.Name = "enemyDamageTakenLabel";
            this.enemyDamageTakenLabel.Size = new System.Drawing.Size(108, 100);
            this.enemyDamageTakenLabel.TabIndex = 0;
            this.enemyDamageTakenLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 57);
            this.label2.TabIndex = 0;
            this.label2.Text = "Equipment:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enemyInventoryLabel
            // 
            this.enemyInventoryLabel.Location = new System.Drawing.Point(1138, 134);
            this.enemyInventoryLabel.Name = "enemyInventoryLabel";
            this.enemyInventoryLabel.Size = new System.Drawing.Size(193, 57);
            this.enemyInventoryLabel.TabIndex = 0;
            this.enemyInventoryLabel.Text = "Equipment:";
            this.enemyInventoryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerEquipmentListBox
            // 
            this.playerEquipmentListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerEquipmentListBox.FormattingEnabled = true;
            this.playerEquipmentListBox.ItemHeight = 29;
            this.playerEquipmentListBox.Items.AddRange(new object[] {
            "Slot 1",
            "Slot 2",
            "Slot 3",
            "Slot 4",
            "Slot 5",
            "Slot 6",
            "Slot 7"});
            this.playerEquipmentListBox.Location = new System.Drawing.Point(12, 194);
            this.playerEquipmentListBox.Name = "playerEquipmentListBox";
            this.playerEquipmentListBox.Size = new System.Drawing.Size(410, 265);
            this.playerEquipmentListBox.TabIndex = 3;
            // 
            // enemyEquipmentListBox
            // 
            this.enemyEquipmentListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyEquipmentListBox.FormattingEnabled = true;
            this.enemyEquipmentListBox.ItemHeight = 29;
            this.enemyEquipmentListBox.Items.AddRange(new object[] {
            "Slot 1",
            "Slot 2",
            "Slot 3",
            "Slot 4",
            "Slot 5",
            "Slot 6",
            "Slot 7"});
            this.enemyEquipmentListBox.Location = new System.Drawing.Point(921, 194);
            this.enemyEquipmentListBox.Name = "enemyEquipmentListBox";
            this.enemyEquipmentListBox.Size = new System.Drawing.Size(410, 265);
            this.enemyEquipmentListBox.TabIndex = 3;
            // 
            // playerBagCapacity
            // 
            this.playerBagCapacity.AutoSize = true;
            this.playerBagCapacity.Location = new System.Drawing.Point(5, 490);
            this.playerBagCapacity.Name = "playerBagCapacity";
            this.playerBagCapacity.Size = new System.Drawing.Size(304, 39);
            this.playerBagCapacity.TabIndex = 4;
            this.playerBagCapacity.Text = "Bag Capacity: 0/20";
            // 
            // takeItemButton
            // 
            this.takeItemButton.Location = new System.Drawing.Point(921, 473);
            this.takeItemButton.Name = "takeItemButton";
            this.takeItemButton.Size = new System.Drawing.Size(410, 56);
            this.takeItemButton.TabIndex = 5;
            this.takeItemButton.Text = "Take Selected Item";
            this.takeItemButton.UseVisualStyleBackColor = true;
            this.takeItemButton.Visible = false;
            this.takeItemButton.Click += new System.EventHandler(this.takeItemButton_Click);
            // 
            // killsLabel
            // 
            this.killsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.killsLabel.Location = new System.Drawing.Point(583, 129);
            this.killsLabel.Name = "killsLabel";
            this.killsLabel.Size = new System.Drawing.Size(177, 34);
            this.killsLabel.TabIndex = 0;
            this.killsLabel.Text = "Kills: 0";
            this.killsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(19F, 38F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1343, 558);
            this.Controls.Add(this.takeItemButton);
            this.Controls.Add(this.playerBagCapacity);
            this.Controls.Add(this.enemyEquipmentListBox);
            this.Controls.Add(this.playerEquipmentListBox);
            this.Controls.Add(this.inventoryButton);
            this.Controls.Add(this.potionButton);
            this.Controls.Add(this.nextEnemyButton);
            this.Controls.Add(this.attackButton);
            this.Controls.Add(this.enemyHealthBar);
            this.Controls.Add(this.playerHealthBar);
            this.Controls.Add(this.enemyHealthLabel);
            this.Controls.Add(this.enemyDamageTakenLabel);
            this.Controls.Add(this.playerDamageTakenLabel);
            this.Controls.Add(this.playerHealthLabel);
            this.Controls.Add(this.enemyLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.enemyInventoryLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.playerLabel);
            this.Controls.Add(this.killsLabel);
            this.Controls.Add(this.depthLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.MaximumSize = new System.Drawing.Size(1361, 605);
            this.MinimumSize = new System.Drawing.Size(1361, 605);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label depthLabel;
        private System.Windows.Forms.Label playerLabel;
        private System.Windows.Forms.Label enemyLabel;
        private System.Windows.Forms.Label playerHealthLabel;
        private System.Windows.Forms.Label enemyHealthLabel;
        private System.Windows.Forms.ProgressBar playerHealthBar;
        private System.Windows.Forms.ProgressBar enemyHealthBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button attackButton;
        private System.Windows.Forms.Button potionButton;
        private System.Windows.Forms.Button nextEnemyButton;
        private System.Windows.Forms.Button inventoryButton;
        private System.Windows.Forms.Label playerDamageTakenLabel;
        private System.Windows.Forms.Label enemyDamageTakenLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label enemyInventoryLabel;
        private System.Windows.Forms.ListBox playerEquipmentListBox;
        private System.Windows.Forms.ListBox enemyEquipmentListBox;
        private System.Windows.Forms.Label playerBagCapacity;
        private System.Windows.Forms.Button takeItemButton;
        private System.Windows.Forms.Label killsLabel;
    }
}

