﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rpg
{
    public partial class Form1 : Form
    {
        GameManager game;
        InventoryForm inv;

        public Form1()
        {
            InitializeComponent();
            game = new GameManager();
            playerHealthBar.Maximum = game.Player.MaxHealth;
            playerHealthLabel.Text = game.Player.CurrentHealth.ToString();
            enemyHealthLabel.Text = game.Enemy.CurrentHealth.ToString();
            enemyHealthBar.Maximum = game.Enemy.MaxHealth;
            enemyLabel.Text = game.Enemy.Name;
            InventoryUpdate();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Program.mainMenu.Close();
            base.OnFormClosing(e);
        }

        private void attackButton_Click(object sender, EventArgs e)
        {
            // Disable attack button and potion button
            attackButton.Enabled = false;
            potionButton.Enabled = false;
            // Hide label showing damage taken by player
            playerDamageTakenLabel.Hide();
            // Player deals damage to Enemy
            int damage = game.Attack(game.Player, game.Enemy);
            // Update label that displays amount of damage taken
            enemyDamageTakenLabel.Show();
            enemyDamageTakenLabel.Text = "-" + damage.ToString();
            enemyDamageTakenLabel.ForeColor = System.Drawing.Color.Red;
            // Update display of enemy health
            enemyHealthBar.Value = game.Enemy.CurrentHealth;
            enemyHealthLabel.Text = game.Enemy.CurrentHealth.ToString();

            if (game.Enemy.IsDead)
            {
                ++game.Kills;
                enemyLabel.Font = new Font(enemyLabel.Font, FontStyle.Strikeout);
                attackButton.Hide();
                potionButton.Hide();
                enemyInventoryLabel.Text = "Loot:";
                takeItemButton.Show();
                attackButton.Enabled = true;
                // Move non natural items from enemy's Equipped to enemy's Bag
                for (int i = 0; i < 6; ++i)
                {
                    if (game.Enemy.Equipped[(InventorySlotId)i] != null)
                    {
                        if (game.Enemy.Equipped[(InventorySlotId)i] is IArmor)
                        {
                            IArmor temp = (IArmor)game.Enemy.Equipped[(InventorySlotId)i];
                            if (!temp.IsNatural)
                            {
                                game.Enemy.Bag.AddItem(game.Enemy.Equipped.Unequip((InventorySlotId)i));
                            }
                        }
                        else if (game.Enemy.Equipped[(InventorySlotId)i] is IWeapon)
                        {
                            IWeapon temp = (IWeapon)game.Enemy.Equipped[(InventorySlotId)i];
                            if (!temp.IsNatural)
                            {
                                game.Enemy.Bag.AddItem(game.Enemy.Equipped.Unequip((InventorySlotId)i));
                            }
                        }
                        else
                        {
                            game.Enemy.Bag.AddItem(game.Enemy.Equipped.Unequip((InventorySlotId)i));
                        }
                    }
                }
                InventoryUpdate();

                nextEnemyButton.Show();
                inventoryButton.Show();
                return;
            }

            System.Threading.ThreadPool.QueueUserWorkItem(delegate // things in this block run in a separate thread from the UI
            {
                System.Threading.Thread.Sleep(1000);// Pauses this thread for 1 second
                Invoke(new Action(delegate // Invoke allows the separate thread to manipulate the UI
                {
                    // Hide label showing damage taken by enemy
                    enemyDamageTakenLabel.Hide();
                    // Enemy deals damage to player
                    damage = game.Attack(game.Enemy, game.Player);
                    // Update damage taken label
                    playerDamageTakenLabel.Show();
                    playerDamageTakenLabel.Text = "-" + damage.ToString();
                    playerDamageTakenLabel.ForeColor = System.Drawing.Color.Red;
                    // Update player health display
                    playerHealthBar.Value = game.Player.CurrentHealth;
                    playerHealthLabel.Text = game.Player.CurrentHealth.ToString();
                    // Reenable attack button and potion button
                    attackButton.Enabled = true;
                    InventoryUpdate();

                    // Show gameover if player is dead
                    if (game.Player.IsDead)
                    {
                        game.ShowGameOver();
                    }
                }));
            });


        }

        private void nextEnemyButton_Click(object sender, EventArgs e)
        {
            nextEnemyButton.Hide();
            inventoryButton.Hide();
            game.NextBattle();
            InventoryUpdate();
            takeItemButton.Hide();
            enemyInventoryLabel.Text = "Equipment:";
            enemyLabel.Font = new Font(enemyLabel.Font, FontStyle.Regular);
            enemyHealthLabel.Text = game.Enemy.CurrentHealth.ToString();
            enemyHealthBar.Maximum = game.Enemy.MaxHealth;
            enemyHealthBar.Value = game.Enemy.CurrentHealth;
            enemyLabel.Text = game.Enemy.Name;
            enemyDamageTakenLabel.Hide();
            attackButton.Show();
            potionButton.Show();
        }

        private void potionButton_Click(object sender, EventArgs e)
        {
            // Player drinks equipped potion
            int healing = game.DrinkPotion(game.Player, (IPotion)game.Player.Equipped[InventorySlotId.POTION]);
            // Update label that displays amount of healing done
            playerDamageTakenLabel.Show();
            playerDamageTakenLabel.Text = "+" + healing.ToString();
            playerDamageTakenLabel.ForeColor = System.Drawing.Color.Green;
            // Update display of enemy health
            playerHealthBar.Value = game.Player.CurrentHealth;
            playerHealthLabel.Text = game.Player.CurrentHealth.ToString();
            // Remove potion from player's equipped items
            game.Player.Equipped[InventorySlotId.POTION] = null;

            InventoryUpdate();
        }

        private void takeItemButton_Click(object sender, EventArgs e)
        {
            int index = enemyEquipmentListBox.SelectedIndex;
            if (index != -1)
            {
                if (game.Player.Bag.Count >= game.Player.Bag.Size)
                {
                    playerBagCapacity.ForeColor = System.Drawing.Color.Red;
                    System.Threading.ThreadPool.QueueUserWorkItem(delegate // things in this block run in a separate thread from the UI
                    {
                        System.Threading.Thread.Sleep(1000);// Pauses this thread for 1 second
                        Invoke(new Action(delegate // Invoke allows the separate thread to manipulate the UI
                        {
                            playerBagCapacity.ForeColor = System.Drawing.Color.Black;
                        }));
                    });
                }
                else
                {
                    game.Player.Bag.AddItem(game.Enemy.Bag.RemoveItem(index));
                }
            }
        }

        private void inventoryButton_Click(object sender, EventArgs e)
        {
            inv = new InventoryForm(game.Player.Bag, game.Player.Equipped);
            inv.Show();
            this.Hide();
        }

        // Update form when inventory is updated. It is public so it can be called from other classes
        public void InventoryUpdate()
        {
            // Update potion button
            if (game.Player.Equipped[InventorySlotId.POTION] != null)
            {
                potionButton.Enabled = true;
                IPotion potion = (IPotion)game.Player.Equipped[InventorySlotId.POTION];
                potionButton.Text = "Take Potion (" + potion.HealValue + ")";
            }
            else
            {
                potionButton.Enabled = false;
                potionButton.Text = "Take Potion";
            }

            // Update player equipment list
            playerEquipmentListBox.Items.Clear();
            for (int i = 0; i < 6; ++i)
            {
                if (game.Player.Equipped[(InventorySlotId)i] != null)
                {
                    playerEquipmentListBox.Items.Add(game.Player.Equipped[(InventorySlotId)i].ToString());
                }
            }

            // Update enemy equipment list
            // If the enemy is dead, display the stored items. If it is alive, display equipped items
            if (game.Enemy.IsDead)
            {
                enemyEquipmentListBox.Items.Clear();
                for (int i = 0; i < game.Enemy.Bag.Count; ++i)
                {
                    enemyEquipmentListBox.Items.Add(game.Enemy.Bag[i].ToString());
                }
            }
            else
            {
                enemyEquipmentListBox.Items.Clear();
                for (int i = 0; i < 6; ++i)
                {
                    if (game.Enemy.Equipped[(InventorySlotId)i] != null)
                    {
                        enemyEquipmentListBox.Items.Add(game.Enemy.Equipped[(InventorySlotId)i].ToString());
                    }
                }
            }

            // Update bag capacity
            playerBagCapacity.Text = "Bag Capacity: " + game.Player.Bag.Count + "/" + game.Player.Bag.Size;

            // Update kills label
            killsLabel.Text = "Kills: " + game.Kills;

            // Update depth label
            depthLabel.Text = "Depth: " + game.Depth;
        }
    }
}
