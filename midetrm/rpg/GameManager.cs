﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class GameManager
    {
        Character player;
        Character enemy;
        int kills;
        int depth;
        bool gameOver;

        public Character Player { get { return player; } }
        public Character Enemy { get { return enemy; } }
        public int Kills { get { return kills; } set { kills = value; } }
        public int Depth { get { return depth; } }
        public bool IsGameOver { get { return gameOver; } }

        public GameManager()
        {
            player = new Player();
            enemy = Factory.RandomEnemyGenerator();
            depth = 1;
            gameOver = false;
        }

        public void ShowGameOver()
        {
            Program.gameOver = new GameOverForm();
            Program.gameOver.Show();
            Program.form1.Dispose();
            //Program.form1.Close();
        }

        public int Attack(Character attacker, Character defender)
        {
            int damage = attacker.CalcTotalAttackValue() - defender.CalcTotalDefenseValue();
            if(damage < 0)
            {
                damage = 0;
            }
            defender.TakeDamage(damage);
            return damage;            
        }

        public int DrinkPotion(Character drinker, IPotion potion)
        {
            int healing = drinker.Heal(potion.HealValue);
            return healing;
        }

        public int NextBattle()
        {
            int i = 0;//
            do
            {
                if (Kills % 5 == 0)
                {
                    enemy = Factory.RandomMinibossGenerator();
                }
                else if (Kills % 5 == 1 && Kills != 1)
                {
                    enemy = Factory.RandomEnemyGenerator();
                    ++depth;
                }
                else
                {
                    enemy = Factory.RandomEnemyGenerator();
                }
                ++i;
            } while ((Player.CalcTotalAttackValue() - Enemy.CalcTotalDefenseValue() <= 0) && (Enemy.CalcTotalAttackValue() - Player.CalcTotalDefenseValue() <= 0));
            return i;
        }
    }
}
