﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rpg
{
    public partial class GameOverForm : Form
    {
        public GameOverForm()
        {
            InitializeComponent();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Program.mainMenu.Close();
            base.OnFormClosing(e);
        }

        private void playAgainButton_Click(object sender, EventArgs e)
        {
            Program.form1 = new Form1();
            Program.form1.Show();
            this.Dispose();
        }
    }
}
