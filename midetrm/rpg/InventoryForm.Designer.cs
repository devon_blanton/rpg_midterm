﻿namespace rpg
{
    partial class InventoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.backToCombatButton = new System.Windows.Forms.Button();
            this.equipSelectedButton = new System.Windows.Forms.Button();
            this.unequipSelectedButton = new System.Windows.Forms.Button();
            this.dropFromBagButton = new System.Windows.Forms.Button();
            this.dropFromEquippedButton = new System.Windows.Forms.Button();
            this.bagListBox = new System.Windows.Forms.ListBox();
            this.bagCapacityLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.equippedListBox = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(503, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 64);
            this.label1.TabIndex = 1;
            this.label1.Text = "Inventory";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backToCombatButton
            // 
            this.backToCombatButton.Location = new System.Drawing.Point(498, 161);
            this.backToCombatButton.Name = "backToCombatButton";
            this.backToCombatButton.Size = new System.Drawing.Size(346, 75);
            this.backToCombatButton.TabIndex = 2;
            this.backToCombatButton.Text = "Return to the Battle";
            this.backToCombatButton.UseVisualStyleBackColor = true;
            this.backToCombatButton.Click += new System.EventHandler(this.backToCombatButton_Click);
            // 
            // equipSelectedButton
            // 
            this.equipSelectedButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipSelectedButton.Location = new System.Drawing.Point(470, 242);
            this.equipSelectedButton.Name = "equipSelectedButton";
            this.equipSelectedButton.Size = new System.Drawing.Size(403, 75);
            this.equipSelectedButton.TabIndex = 2;
            this.equipSelectedButton.Text = "--> Equip Selected -->";
            this.equipSelectedButton.UseVisualStyleBackColor = true;
            this.equipSelectedButton.Click += new System.EventHandler(this.equipSelectedButton_Click);
            // 
            // unequipSelectedButton
            // 
            this.unequipSelectedButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unequipSelectedButton.Location = new System.Drawing.Point(470, 323);
            this.unequipSelectedButton.Name = "unequipSelectedButton";
            this.unequipSelectedButton.Size = new System.Drawing.Size(403, 75);
            this.unequipSelectedButton.TabIndex = 2;
            this.unequipSelectedButton.Text = "<-- Unequip Selected <--";
            this.unequipSelectedButton.UseVisualStyleBackColor = true;
            this.unequipSelectedButton.Click += new System.EventHandler(this.unequipSelectedButton_Click);
            // 
            // dropFromBagButton
            // 
            this.dropFromBagButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropFromBagButton.Location = new System.Drawing.Point(88, 445);
            this.dropFromBagButton.Name = "dropFromBagButton";
            this.dropFromBagButton.Size = new System.Drawing.Size(239, 75);
            this.dropFromBagButton.TabIndex = 2;
            this.dropFromBagButton.Text = "Drop Selected";
            this.dropFromBagButton.UseVisualStyleBackColor = true;
            this.dropFromBagButton.Click += new System.EventHandler(this.dropFromBagButton_Click);
            // 
            // dropFromEquippedButton
            // 
            this.dropFromEquippedButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropFromEquippedButton.Location = new System.Drawing.Point(1045, 445);
            this.dropFromEquippedButton.Name = "dropFromEquippedButton";
            this.dropFromEquippedButton.Size = new System.Drawing.Size(239, 75);
            this.dropFromEquippedButton.TabIndex = 2;
            this.dropFromEquippedButton.Text = "Drop Selected";
            this.dropFromEquippedButton.UseVisualStyleBackColor = true;
            this.dropFromEquippedButton.Click += new System.EventHandler(this.dropFromEquippedButton_Click);
            // 
            // bagListBox
            // 
            this.bagListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bagListBox.FormattingEnabled = true;
            this.bagListBox.ItemHeight = 29;
            this.bagListBox.Location = new System.Drawing.Point(12, 55);
            this.bagListBox.Name = "bagListBox";
            this.bagListBox.Size = new System.Drawing.Size(390, 381);
            this.bagListBox.TabIndex = 3;
            // 
            // bagCapacityLabel
            // 
            this.bagCapacityLabel.Location = new System.Drawing.Point(12, 13);
            this.bagCapacityLabel.Name = "bagCapacityLabel";
            this.bagCapacityLabel.Size = new System.Drawing.Size(390, 39);
            this.bagCapacityLabel.TabIndex = 4;
            this.bagCapacityLabel.Text = "Bag Capacity:";
            this.bagCapacityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1079, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 39);
            this.label3.TabIndex = 4;
            this.label3.Text = "Equipped:";
            // 
            // equippedListBox
            // 
            this.equippedListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equippedListBox.FormattingEnabled = true;
            this.equippedListBox.ItemHeight = 29;
            this.equippedListBox.Location = new System.Drawing.Point(997, 55);
            this.equippedListBox.Name = "equippedListBox";
            this.equippedListBox.Size = new System.Drawing.Size(334, 381);
            this.equippedListBox.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(895, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "Helmet:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(850, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 25);
            this.label5.TabIndex = 5;
            this.label5.Text = "Chestpiece:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(889, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Grieves:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(851, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 25);
            this.label7.TabIndex = 5;
            this.label7.Text = "Vambraces:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(882, 158);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 25);
            this.label8.TabIndex = 5;
            this.label8.Text = "Weapon:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(903, 183);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 25);
            this.label9.TabIndex = 5;
            this.label9.Text = "Potion:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InventoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(19F, 38F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1343, 558);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bagCapacityLabel);
            this.Controls.Add(this.equippedListBox);
            this.Controls.Add(this.bagListBox);
            this.Controls.Add(this.dropFromEquippedButton);
            this.Controls.Add(this.dropFromBagButton);
            this.Controls.Add(this.unequipSelectedButton);
            this.Controls.Add(this.equipSelectedButton);
            this.Controls.Add(this.backToCombatButton);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.MaximumSize = new System.Drawing.Size(1361, 605);
            this.MinimumSize = new System.Drawing.Size(1361, 605);
            this.Name = "InventoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InventoryForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button backToCombatButton;
        private System.Windows.Forms.Button equipSelectedButton;
        private System.Windows.Forms.Button unequipSelectedButton;
        private System.Windows.Forms.Button dropFromBagButton;
        private System.Windows.Forms.Button dropFromEquippedButton;
        private System.Windows.Forms.ListBox bagListBox;
        private System.Windows.Forms.Label bagCapacityLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox equippedListBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}