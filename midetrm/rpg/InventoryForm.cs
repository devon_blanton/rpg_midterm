﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rpg
{
    partial class InventoryForm : Form
    {
        StoredItems bag;
        EquippedItems equipped;
        public InventoryForm(StoredItems bag, EquippedItems equipped)
        {
            InitializeComponent();
            this.bag = bag;
            this.equipped = equipped;
            InventoryUpdate();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Program.mainMenu.Close();
            base.OnFormClosing(e);
        }

        private void equipSelectedButton_Click(object sender, EventArgs e)
        {
            int index = bagListBox.SelectedIndex;
            InventorySlotId equipmentSlot;
            if(index != -1)
            {
                equipmentSlot = bag[index].Slot;
                if(equipped[equipmentSlot] != null)
                {
                    bag.AddItem(equipped.Equip(bag.RemoveItem(index)));
                }
                else
                {
                    equipped.Equip(bag.RemoveItem(index));
                }
            }
            InventoryUpdate();
        }

        private void dropFromBagButton_Click(object sender, EventArgs e)
        {
            int index = bagListBox.SelectedIndex;
            if(index != -1)
            {
                bag.RemoveItem(index);
                InventoryUpdate();
            }
        }

        private void dropFromEquippedButton_Click(object sender, EventArgs e)
        {
            int index = equippedListBox.SelectedIndex;
            if (index != -1)
            {
                equipped.Unequip((InventorySlotId)index);
                InventoryUpdate();
            }
        }

        private void unequipSelectedButton_Click(object sender, EventArgs e)
        {
            int index = equippedListBox.SelectedIndex;
            if(index != -1 && equipped[(InventorySlotId)index] != null)
            {
                if (bag.Count >= bag.Size)
                {
                    bagCapacityLabel.ForeColor = System.Drawing.Color.Red;
                    System.Threading.ThreadPool.QueueUserWorkItem(delegate // things in this block run in a separate thread from the UI
                    {
                        System.Threading.Thread.Sleep(1000);// Pauses this thread for 1 second
                        Invoke(new Action(delegate // Invoke allows the separate thread to manipulate the UI
                        {
                            bagCapacityLabel.ForeColor = System.Drawing.Color.Black;
                        }));
                    });
                }
                else
                {
                    bag.AddItem(equipped.Unequip((InventorySlotId)index));
                }
            }
            InventoryUpdate();
        }

        private void backToCombatButton_Click(object sender, EventArgs e)
        {
            Program.form1.InventoryUpdate();
            Program.form1.Show();
            this.Dispose();
        }

        public void InventoryUpdate()
        {
            // Update bag capacity display
            bagCapacityLabel.Text = "Bag Capacity: " + bag.Count + "/" + bag.Size;

            // Update items listed in bag
            bagListBox.Items.Clear();
            for(int i = 0; i < bag.Count; ++i)
            {
                bagListBox.Items.Add(bag.GetItem(i));
            }

            // Update equipped items listed
            equippedListBox.Items.Clear();
            for(int i = 0; i < 6; ++i)
            {
                if(equipped[(InventorySlotId)i] != null)
                {
                    equippedListBox.Items.Add(equipped[(InventorySlotId)i]);
                }
                else
                {
                    equippedListBox.Items.Add("");
                }
            }
        }
    }
}
