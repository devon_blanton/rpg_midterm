﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    abstract class Item
    {
        protected string name;
        protected InventorySlotId slot;
        public string Name { get { return name; } }
        public InventorySlotId Slot { get { return slot; } }


        //Maybe add IComparable to class and CompareTo method? See original UML
        //Maybe add Equals override and GetHashCode override

        public override string ToString()
        {
            string returnValue = Name;
            if (this is IArmor temp1)
            {
                returnValue += " (+" + temp1.DefenseValue + " Defense) ";
            }
            if (this is IWeapon temp2)
            {
                returnValue += " (+" + temp2.AttackValue + " Attack) ";
            }
            if (this is IPotion temp3)
            {
                returnValue += " (+" + temp3.HealValue + " Healing)";
            }
            return returnValue;
        }
    }
}
