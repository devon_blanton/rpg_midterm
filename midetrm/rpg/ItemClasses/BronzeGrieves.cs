﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class BronzeGrieves : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public BronzeGrieves()
        {
            Random rand = new Random();
            name = "Bronze Grieves";
            slot = InventorySlotId.GRIEVES;
            DefenseValue = rand.Next(0, 4);
            IsNatural = false;
        }
    }
}
