﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class BronzeHelmet : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public BronzeHelmet()
        {
            Random rand = new Random();
            name = "Bronze Helmet";
            slot = InventorySlotId.HELMET;
            DefenseValue = rand.Next(0,4);
            IsNatural = false;
        }
    }
}
