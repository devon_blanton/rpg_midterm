﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class BronzeSword : Item, IWeapon
    {
        public int AttackValue { get; }
        public bool IsNatural { get; }

        public BronzeSword()
        {
            Random rand = new Random();
            name = "Bronze Sword";
            slot = InventorySlotId.WEAPON;
            AttackValue = rand.Next(4, 11);
            IsNatural = false;
        }
    }
}
