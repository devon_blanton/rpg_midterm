﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class BronzeVambraces : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public BronzeVambraces()
        {
            Random rand = new Random();
            name = "Bronze Vambraces";
            slot = InventorySlotId.VAMBRACES;
            DefenseValue = rand.Next(0, 4);
            IsNatural = false;
        }
    }
}
