﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Buckler : Item, IWeapon, IArmor
    {
        public int AttackValue { get; }
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public Buckler()
        {
            Random rand = new Random();
            name = "Buckler";
            slot = InventorySlotId.WEAPON;
            AttackValue = rand.Next(3, 7);
            DefenseValue = rand.Next(4, 11);
            IsNatural = false;
        }
    }
}
