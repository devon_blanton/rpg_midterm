﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class Gambeson : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public Gambeson()
        {
            Random rand = new Random();
            name = "Gambeson";
            slot = InventorySlotId.CHESTPIECE;
            DefenseValue = rand.Next(0, 4);
            IsNatural = false;
        }
    }
}
