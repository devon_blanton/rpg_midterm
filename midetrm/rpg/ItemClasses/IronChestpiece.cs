﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class IronChestpiece : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public IronChestpiece()
        {
            Random rand = new Random();
            name = "Iron Chestpiece";
            slot = InventorySlotId.CHESTPIECE;
            DefenseValue = rand.Next(2, 6);
            IsNatural = false;
        }
    }
}
