﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class IronGrieves : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public IronGrieves()
        {
            Random rand = new Random();
            name = "Iron Grieves";
            slot = InventorySlotId.GRIEVES;
            DefenseValue = rand.Next(2, 6);
            IsNatural = false;
        }
    }
}
