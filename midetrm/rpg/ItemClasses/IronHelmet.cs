﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class IronHelmet : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public IronHelmet()
        {
            Random rand = new Random();
            name = "Iron Helmet";
            slot = InventorySlotId.HELMET;
            DefenseValue = rand.Next(2, 6);
            IsNatural = false;
        }
    }
}
