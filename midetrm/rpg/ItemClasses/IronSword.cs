﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class IronSword : Item, IWeapon
    {
        public int AttackValue { get; }
        public bool IsNatural { get; }

        public IronSword()
        {
            Random rand = new Random();
            name = "Iron Sword";
            slot = InventorySlotId.WEAPON;
            AttackValue = rand.Next(8, 15);
            IsNatural = false;
        }
    }
}
