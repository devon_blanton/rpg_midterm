﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class IronVambraces : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public IronVambraces()
        {
            Random rand = new Random();
            name = "Iron Vambraces";
            slot = InventorySlotId.VAMBRACES;
            DefenseValue = rand.Next(2, 6);
            IsNatural = false;
        }
    }
}
