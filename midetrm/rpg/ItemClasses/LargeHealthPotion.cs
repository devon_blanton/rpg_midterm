﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class LargeHealthPotion : Item, IPotion
    {
        public int HealValue { get; }

        public LargeHealthPotion()
        {
            Random rand = new Random();
            name = "Large Health Potion";
            slot = InventorySlotId.POTION;
            HealValue = rand.Next(20, 26);
        }
    }
}
