﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class NaturalArmor : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public NaturalArmor(int minDefense, int maxDefense)
        {
            Random rand = new Random();
            name = "Natural Armor";
            slot = InventorySlotId.CHESTPIECE;
            DefenseValue = rand.Next(minDefense, maxDefense+1);
            IsNatural = true;
        }
    }
}
