﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class NaturalWeapon : Item, IWeapon
    {
        public int AttackValue { get; }
        public bool IsNatural { get; }

        public NaturalWeapon(int minAttack, int maxAttack)
        {
            Random rand = new Random();
            name = "Natural Weapon";
            slot = InventorySlotId.WEAPON;
            AttackValue = rand.Next(minAttack, maxAttack + 1);
            IsNatural = true;
        }
    }
}
