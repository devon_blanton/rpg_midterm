﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class SmallHealthPotion : Item, IPotion
    {
        public int HealValue { get; }

        public SmallHealthPotion()
        {
            Random rand = new Random();
            name = "Small Health Potion";
            slot = InventorySlotId.POTION;
            HealValue = rand.Next(10, 16);
        }
    }
}
