﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class SteelChestpiece : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public SteelChestpiece()
        {
            Random rand = new Random();
            name = "Steel Chestpiece";
            slot = InventorySlotId.CHESTPIECE;
            DefenseValue = rand.Next(4, 8);
            IsNatural = false;
        }
    }
}

