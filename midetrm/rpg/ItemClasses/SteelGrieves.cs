﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class SteelGrieves : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public SteelGrieves()
        {
            Random rand = new Random();
            name = "Steel Grieves";
            slot = InventorySlotId.GRIEVES;
            DefenseValue = rand.Next(4, 8);
            IsNatural = false;
        }
    }
}
