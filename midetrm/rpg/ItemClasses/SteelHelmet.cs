﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class SteelHelmet : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public SteelHelmet()
        {
            Random rand = new Random();
            name = "Steel Helmet";
            slot = InventorySlotId.HELMET;
            DefenseValue = rand.Next(4, 8);
            IsNatural = false;
        }
    }
}
