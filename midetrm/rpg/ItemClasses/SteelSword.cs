﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class SteelSword : Item, IWeapon
    {
        public int AttackValue { get; }
        public bool IsNatural { get; }

        public SteelSword()
        {
            Random rand = new Random();
            name = "Steel Sword";
            slot = InventorySlotId.WEAPON;
            AttackValue = rand.Next(12, 19);
            IsNatural = false;
        }
    }
}
