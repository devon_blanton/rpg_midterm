﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class SteelVambraces : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public SteelVambraces()
        {
            Random rand = new Random();
            name = "Steel Vambraces";
            slot = InventorySlotId.VAMBRACES;
            DefenseValue = rand.Next(4, 8);
            IsNatural = false;
        }
    }
}
