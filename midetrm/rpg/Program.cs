﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rpg
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static MainMenuForm mainMenu;
        public static Form1 form1;
        public static GameOverForm gameOver;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            mainMenu = new MainMenuForm();
            form1 = new Form1();
            Application.Run(mainMenu);
        }
    }
}
