﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg
{
    class StoredItems
    {
        Item[] items;
        int count;

        public int Count { get { return count; } }
        public int Size { get; }

        public StoredItems(int size)
        {
            items = new Item[size];
            Size = size;
            count = 0;
        }

        public StoredItems(int size, int random) : this(size)
        {
            if (random > size)
            {
                random = size;
            }
            for (int i = 0; i < random; ++i)
            {
                items[i] = Factory.RandomItemGenerator();
                ++count;
            }
        }

        public StoredItems(int size, Item[] items)
        {
            this.items = new Item[size];
            this.count = items.Length;

            if (this.items.Length < items.Length)
            {
                count = this.items.Length;
            }

            for (int i = 0; i < this.items.Length; ++i)
            {
                this.items[i] = items[i];
            }
        }


        public Item GetItem(int index)
        {
            return items[index];
        }

        public void AddItem(Item item)
        {
            items[count] = item;
            ++count;
            Program.form1.InventoryUpdate();
        }

        public Item RemoveItem(int index)
        {
            Item returnItem = items[index];
            items[index] = null;
            for (int i = index; i < items.Length; ++i)
            {
                if (i == items.Length -1 || items[i + 1] == null)
                {
                    break;
                }

                items[i] = items[i + 1];
                items[i + 1] = null;
            }
            --count;

            Program.form1.InventoryUpdate();
            return returnItem;
        }

        public Item this[int index]
        {
            get
            {
                return GetItem(index);
            }

            set
            {
                items[index] = value;
                Program.form1.InventoryUpdate();
            }
        }
    }
}
